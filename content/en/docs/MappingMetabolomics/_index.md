---
tags: ["docs","mapping", "metabolomics", "tuto"] 
title: "Mapping of Metabolomics data"
linkTitle: "Mapping of Metabolomics data"
weight: 5
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/mapping" >}}


### Objectives

The aim of this section is to **learn how to map metabolomics data to a metabolic network**, meaning pinpointing metabolites identified in experiments within metabolic networks. 
As explained [here](../dataset), we will use the metabolites annotated at level 1 (MSI standard) and with Mann & Withney p-value <0.05 between control patients and patients with HE as a benchmark. **The metabolic fingerprint contains 28 metabolites.**

**In metabolic network reconstructions, metabolites are referenced with names and with identifiers that are specific to each network** (or each database they are issued from). For instance, in the different versions of Recon2, metabolite identifiers start by "M_" and end with a letter corresponding to the cellular compartment (e.g., "_c" for cytosol, "_m" for mitochondria …).

Metabolite names used in networks and in experimental datasets do not usually comply with any specific naming convention and come with different spellings, lower or upper-case letters… Therefore, it is often impossible or unsuccessful to perform a direct and automatic matching between metabolite names or identifiers present in the network and metabolite names provided by experimentalists.

**In metabolic networks, other identifiers coming from metabolite databases such as KEGG, ChEBI, HMDB, LipidMaps … can also be used for each metabolite. These identifiers can be used to make the link between metabolites in an experimental dataset and metabolites in metabolic networks, but this requires finding such database identifiers for the dataset metabolites first.**

![Link Metabolites](/metexplore-web/metexplore-training/images/linkMetabolites.png)

*How to link metabolites in an experimental dataset with metabolites in metabolic networks*

{{% importantParagraph %}}
To make the link between the metabolites present in the HE dataset and the metabolites in the human metabolic network, you will therefore need to 

1. find corresponding database identifiers for the HE dataset metabolites
2. match them with the ones present in the metabolic network.
{{% /importantParagraph %}}

{{< nextButton link="NameBasedMapping" >}}
