---
tags: ["docs","mapping", "metabolomics", "tuto"] 
title: "Identify BioSource metabolite ids from external database ids"
linkTitle: "Identify BioSource metabolite ids"
weight: 3
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/matcher" >}}


To be able to map data metabolites to a metabolic network, it is necessary to find their corresponding identifiers in the network. This can be done using the "metabolite identifier matcher" tool in MetExplore. It proceeds by matching the database identifiers retrieved for data metabolites (used as input) with the ones present in the network.

{{% warning %}}
Note than before doing any matching or mapping it is better to remove all filters and searches that you have done.

In the filtered or searched grids, right click and select "Delete Filter & Search"
{{% /warning %}}

{{% tutoParagraph %}}

Select **Toolbox - Metabolite identifier matcher** in the menu at the top of the page: this will open a "metabolite identifier" window.

![Matcher menu](/metexplore-web/metexplore-training/images/matcherMenu.png)
*Finding the network metabolite identifiers corresponding to the data metabolites in MetExplore*

- In this window, you can simply paste your list of data metabolites with their corresponding database identifier.

- Copy the table built in the previous step (including the header row).**Be careful, the first column header must be changed to "dataset_name"**

- Check the "consider first row of columns as headers" box to indicate that the first line of your copied dataset corresponds to the type of identifiers

- Do CTRL+V on the first cell of the grid.

The headers should be automatically completed. If not choose the identifier corresponding to each of your columns in the drop-down menu.

{{% /tutoParagraph %}}

![Metabolite identifier](/metexplore-web/metexplore-training/images/metaboliteIdentifier.png)

*"Metabolite Identifier Matcher" tool in MetExplore*

**The "metabolite identifier matcher" tool allows you to perform 2 types of matching: a standard "exact" matching and a "class" matching.**

- The "exact" matching will try to match exactly the identifiers that you provide with the ones informed in the BioSource.
- The "class" matching will try to find more generic compounds corresponding to the sub-class, class, family … of the metabolite provided as input in case no exact match is found. 

{{% tutoParagraph %}}
Click on the "Match Identifier" button.

Note that you can resize the result grid.
{{% /tutoParagraph %}}

You will see, as shown in the following screenshot that not all metabolites are retrieved (17/28). In fact, the exact match will only retrieve the ones with exactly the same ChEBI.

It is already better than the name based matching but still incomplete.

![Metabolite identifier result](/metexplore-web/metexplore-training/images/metaboliteIdentifierResult.png)
*Results of the exact matching in MetExplore*

Indeed, in the metabolic networks, for some groups of molecules, each specific molecular species is not detailed but only the global family or class is present. This is especially true for lipids. 

For example, in Recon2.2, all specific triglycerides (TG(16:0/16:0/16:0), CHEBI:77393; TG(18:0/16:0/16:0), CHEBI: 89751; …) are not detailed but are instead regrouped under the generic triglyceride class ("tag_hs", CHEBI:17855).

By checking the **"class identifier for chebi"** box, it is possible to **find the more generic "class" metabolites corresponding to specific data metabolites, if these specific metabolites are not detailed in the network (and their generic class is present)**. This is done based on the ChEBI and Lipidmaps identifiers, using the ontology defined in these databases. The distance from the dataset metabolites to the matched network metabolite is reported in the result table.

For example, the specific triglyceride TG(16:1/16:1/16:1) (CHEBI:75841), which could be identified from lipidomics analyses, will match with the global triglyceride "M_tag_hs" (CHEBI:17855) in the network, with a distance of 1.

![Class Matching](/metexplore-web/metexplore-training/images/classMatching.png)

*Class matching in MetExplore: example of the TG(16:1/16:1/16:1) metabolite*

The specific triglyceride TG(16:0/16:0/18:1) (CHEBI:138453) from the dataset will also match with the global triglyceride "M_tag_hs" (CHEBI:17855) in the network, but with a distance of 2.

![Class Matching 2](/metexplore-web/metexplore-training/images/classMatching2.png)

*Class matching in MetExplore: example of the TG(16:0/16:0/18:1) metabolite*

There is also the specific case, where **the anionic form (base) of the fatty acid is present in the network whereas the acid form in present in the dataset**. In this case, the base or acid form can be retrieved when using the "class" matching option. It will result in a matching distance of "0.1".

For instance, the pyridoxic acid (in the dataset) is only present as "pyridoxate" in the metabolic network.

![Class Matching 3](/metexplore-web/metexplore-training/images/classMatching3.png)

*Class matching in MetExplore: example of the "pyridoxic acid" metabolite*


{{% tutoParagraph %}}

Perform a new matching now ticking the box "class identifier".

{{% /tutoParagraph %}}

![Metabolite identifier by class result](/metexplore-web/metexplore-training/images/metaboliteIdentifierClassResult.png)

*Results of the ChEBI ontology-based mapping in MetExplore*

Now 27 out of the 28 metabolites are matched. New columns provide information on the type of matching as well as the distance in the ontology.

Only "Indolelactic acid" is not mapped.


{{% tutoParagraph %}}

Save the result file

{{% /tutoParagraph %}}

Results can be exported as an xlsx file by clicking on the "Save Results in File" button.

**The xlsx file contains 2 sheets:**

- The **"Dataset" sheet**, which contains the same columns as displayed in the MetExplore interface: the initial dataset list of metabolites ("dataset_name" and "dataset_chebi") with 3 additional columns:
  - **"result_BioSourceIdentifier"**: contains the corresponding identifiers in the metabolite network. 
  - **"result_distance"**: corresponds to the matching distances between the network identifier and each dataset identifer. ".1" corresponds to metabolites that are matched with the acid/base form.
  - **"result_matchingType"**: indicates if the matching is exact or not.
- If several dataset identifiers match the network identifier, these identifiers (as well as the corresponding "distance" and "matching_type") are separated by ";".
Two complementary columns are also added in the case of a "class" matching:
  - **"result_nbMapped"**: indicates the number of network metabolites that match with each dataset metabolite.
  - **"result_averageDistance"**: is the average distance of all network metabolites that match with each dataset metabolite.
- The **"Metabolites"** sheet contains all the network metabolites with their name and identifier in the 2 first columns ("BioSource_name" & "BioSource_Identifier") and 6 additional columns:
  - **"dataset_name"** contains the names of the dataset metabolite names that match with the network metabolites. If there are multiple data metabolites matching on the same network metabolite, they are separated by a semi-colon ";"
  - **"distance"** indicates the matching distance for each metabolite in the "dataset_name" column, with the same order and also separated by a semi-colon in case of multiple matches;
  - **"nbdataset_matched"** indicates the number of dataset metabolites that match with the network metabolite (corresponds to the number of metabolites listed in the "dataset_name" column;
  - **"average_distance"** is the mean of the matching distances of all dataset metabolites that match with the network metabolites (i.e., mean of the distances listed in the "distance" column).

In the case of one dataset metabolite matching several times with the same network metabolite (because different identifiers are provided as input for the same metabolite) only the match with the minimal distance is kept in the "Metabolites" table. 

{{< nextButton link="MapIds" >}}
