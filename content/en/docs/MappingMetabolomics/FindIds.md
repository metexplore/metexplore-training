---
tags: ["docs","mapping", "metabolomics", "tuto"] 
title: "Find identifiers"
linkTitle: "Find identifiers"
weight: 2
---

**The first step consists in finding different database identifiers for the metabolites in your dataset**, by searching in the different public databases such as [Lipid Maps](https://www.lipidmaps.org/), [ChEBI](https://www.ebi.ac.uk/chebi/), [HMDB](http://www.hmdb.ca/), etc … Conversion tools can also be useful for this purpose, for example the [Compound ID Conversion tool of MetaboAnalyst](https://www.metaboanalyst.ca/MetaboAnalyst/upload/ConvertView.xhtml) converts metabolite names into various identifiers.

### Search identifiers for the HE dataset metabolites using MetaboAnalyst


{{% tutoParagraph %}}

- Go to [MetaboAnalyst](https://www.metaboanalyst.ca/)
Click on "Click here to start", select the "other utilities" module, choose "Compound ID conversion"
- Copy paste the list of metabolite names from the [HE dataset](/metexplore-web/metexplore-training/data/HE-fingerprint.xlsx) and click submit (check that the "specify input type" is set to "Common Name")

{{% /tutoParagraph %}}

<details>

<summary>
	MetaboAnalyst identification (click me!)
</summary>

![MetaboAnalyst form](/metexplore-web/metexplore-training/images/metaboAnalyst.png)

![MetaboAnalyst result](/metexplore-web/metexplore-training/images/metaboAnalystResult.png)

*Conversion of metabolite identifiers in MetaboAnalyst*

</details>

Note that for some metabolites, there are several different possible identifiers, such as for "Acetyl-glucosamine" or "Acetyl-alanine". For these metabolites, you can view the different matches found by clicking on "view", and select the one that corresponds to your metabolite (if any). For some other metabolites, no CHEBI identifier has been found. For these later metabolites, you need to perform a manual search in the ChEBI database.

{{% tutoParagraph %}}

- Select the good match (if any) for Acetyl-glucosamine" and "Acetyl-alanine"
- Scroll down and click on the download button to get a CSV file.

If there is a problem or if you are completely lost, get the result file **[here](/metexplore-web/metexplore-training/data/MetaboAnalystResults.csv)**

{{% /tutoParagraph %}}




### Search identifiers for the HE dataset metabolites using ChEBI database

We are going to work on the metabolites with no ChEBI identifiers found by MetaboAnalyst

{{% tutoParagraph %}}

- Go to [chebi](https://www.ebi.ac.uk/chebi/)
- For each of these metabolites, enter its name in the search bar
- Check manually each entry to select the ones that truly correspond to the metabolite that you search for

You can look at the "Synonyms" section to check for other possible names for each entry, and check that "Acetyl-glucosamine" is included. 
You might also find some corresponding identifiers for other databases in the "Manual Xrefs" section.

{{% /tutoParagraph %}}

<details>

<summary>Results for Acetyl-glucosamine</summary>

![Chebi results](/metexplore-web/metexplore-training/images/acetylGlucosamine.png)

*Results of chebi search for acetyl glucosamine*

</details>

{{% tutoParagraph %}}

- Open the csv file obtained with MetaboAnalyst. In excel you may have to use the DATA->convert function to specify that “,” is the separator between the two columns.
- Update the file with the extra CHEBI

If there is a problem or if you are completely lost, get the result file **[here](/metexplore-web/metexplore-training/data/MetaboAnalystResultsCompleted.csv)**.

{{% /tutoParagraph %}}

![MetaboAnalyst csv results completed](/metexplore-web/metexplore-training/images/metaboCsvCompleted.png) 

*Chebi identifiers identified by MetaboAnalyst and by the Chebi website (in red)*

Now that we have all the ChEBI identifiers, we can move to the matching step consisting in finding the corresponding identifiers in the genome scale metabolic network.

{{< nextButton link="IdentifyIds" >}}
