---
tags: ["docs","mapping", "metabolomics", "tuto"] 
title: "Name based mapping"
linkTitle: "Name based mapping"
weight: 1
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/mapping" >}}

To highlight the limitation of name-based mapping, we are going to perform the mapping from the 28 names of the HE dataset in MetExplore.
The mapping can be performed in MetExplore using the Omics menu.

{{% tutoParagraph %}}

1. In MetExplore, once you have selected the #4311 Biosource, start a new mapping by clicking on menu "Omics->Mapping->From Omics"
2. Select the "name" as the feature to be used for the mapping
3. Copy paste the list of metabolite names in the table (data available **[here](/metexplore-web/metexplore-training/data/HE-fingerprint.xlsx)**)

{{% /tutoParagraph %}}

<details>

<summary>Results (click me!)</summary>

![Name mapping](/metexplore-web/metexplore-training/images/metaboliteMapping.png)

*Metabolite mapping result*

No metabolites will be mapped in that way. It means that no metabolite in the list has exactly the same name in the dataset and in the network. **This highlights the clear limitation of name-based mapping.**

</details>

{{< nextButton link="FindIds" >}}