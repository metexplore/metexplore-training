---
tags: ["docs","mapping", "metabolomics", "tuto"] 
title: "Identify pathways and reactions linked to metabolite mapping"
linkTitle: "Identify pathways and reactions linked to metabolite mapping"
weight: 5
---


{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/mapping" >}}


Results of the mapping are also displayed in the “pathways”, “reactions”, and “metabolites” grids of the network data, where a new column named with the mapping name is added.

### Mapping result in the metabolite grid

In the “Metabolites” grid, this main column is subdivided into several sub-columns: the first one ("identified") indicates if the metabolite was in the dataset, and the next ones correspond to the values ​​of the different conditions. 

{{% tutoParagraph %}}

Sort the "identified" column to display the mapped metabolites at the top of the grid.

{{% /tutoParagraph %}}

<details>

<summary>How to Sort results of the mapping in the metabolite grid? (click me!)</summary>

![Mapping result in metabolite grid](/metexplore-web/metexplore-training/images/mappingResultMetaboliteGrid.png)

*Results of the metabolite mapping displayed on the metabolites grid in MetExplore


</details>

### Mapping result in the reaction grid

In the “Reactions” grid, the added column for mapping results indicates, for each reaction, the number of metabolites that are present in the HE dataset.

{{% tutoParagraph %}}

Find the number of metabolites and the coverage of these two reactions:

- asparagine synthase (glutamine-hydrolysing)
- intracellular transport reaction of bile acid (“R_TCHOLAtx” reaction) 

{{% /tutoParagraph %}}

<details>
<summary>
Mapping result on reaction grid (click me!)
</summary>

The “asparagine synthase (glutamine-hydrolysing)” reaction uses 3 metabolites that are present in the dataset: “M_asn_L_c”, “M_glu_L_c” and “M_gln_L_c” among the 9 metabolites that are either consumed or produced by this reaction, leading to a coverage of 33% by the data. Similarly, 2 dataset metabolites are mapped on the intracellular transport reaction of bile acid (“R_TCHOLAtx” reaction) out of the 2 metabolites used by this reaction (coverage = 100%).


![Mapping result in reaction grid](/metexplore-web/metexplore-training/images/mappingResultReactionGrid.png)

*Results of the metabolite mapping displayed on the reactions grid in MetExplore
*
</details>

### Mapping result in the pathway grid

In the “Pathways” grid, the added column indicates the number of data metabolites that are found in each pathway (“Nb of mapped”) and the proportion of the pathway that these mapped metabolites represent (“coverage”). 

**The over-representation of the mapped metabolites in each pathway is tested using a Right tailed Fisher Exact Test (“p-value” column). The p-values are corrected to account for the multiple tests performed for all pathways.** Bonferroni and Benjamini-Hochberg corrected p-values are presented in the grid. *** indicates a p-value < 0.0001; **indicates a p-value < 0.001; *indicates a p-value < 0.05.

Note that the results of this pathway over-representation analysis gives you an idea about the global localization of your dataset metabolites. However, you need to be careful when interpreting these results in terms of statistical representativeness as they might be biased by the type of analyses that the data are issued from. Indeed, not all metabolites can been identified by the metabolomics analyses (and a fortiori by the lipidomics analyses) so that some pathways may not be covered at all by your analyses whereas others will be artificially over-covered. 

{{% tutoParagraph %}}

Find the 5 most enriched pathways considering the BH p-value.

{{% /tutoParagraph %}}

<details>
<summary>Results of the metabolite mapping on the pathway grid (click me!)</summary>


![Mapping result in pathway grid](/metexplore-web/metexplore-training/images/mappingResultPathwayGrid.png)

*Results of the metabolite mapping displayed on the pathways grid in MetExplore*

</details>

{{< nextButton link="Visualise" >}}
