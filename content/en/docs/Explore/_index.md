---
# categories: ["Examples", "Placeholders"]
tags: ["docs"] 
title: "Explore metabolic networks"
linkTitle: "Explore"
weight: 4
description: >
  Explore metabolic networks with MetExplore
---

### Objectives

The aim of this section is to describe how you can explore the content of a metabolic network and get familiar with the MetExplore interface.

{{< nextButton link="WhatIsABioSource" >}}