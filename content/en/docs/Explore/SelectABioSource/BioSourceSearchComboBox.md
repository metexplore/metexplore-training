---
tags: ["docs","biosource", "tuto"] 
title: "BioSource search with the combo box"
linkTitle: "BioSource search with the combo box"
weight: 7
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

The right panel of MetExplore contains BioSource information. 
You can use the combo box "Selected BioSource" to search BioSources. 

{{% tutoParagraph %}}

Find the BioSource that we want to explore ([reminder]({{< relref "_index.md#biosourceTuto" >}})) and click on it to select it.

{{% /tutoParagraph %}}

<details>

<summary>Screenshot of how to do (click-me !)</summary>

![BioSource ComboBox](/metexplore-web/metexplore-training/images/bioSourceComboBox.png)

*Selection of a BioSource using the right panel in MetExplore*

</details>

{{< nextButton link="ExploreMetabolicContent" >}}
