---
tags: ["docs","biosource", "tuto"] 
title: "BioSource search based on search field"
linkTitle: "BioSource search based on search field"
weight: 7
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

By clicking on the arrow at the top of each column, you can perform a search.

{{% tutoParagraph %}}

- Add the "organism" column
- Click on the arrow next to Organism and it will start filtering the column.
- Once you have found your BioSource of interest in the list, you need to select it to load all the content of the BioSource and start working on it.
To do so you can:
  - Right click on the line and click on "Select BioSource"
  - Double click on the line

{{% /tutoParagraph %}}

{{% warning %}}

Be careful, the organism field is not searchable for the moment due to a bug in MetExplore!

{{% /warning %}}

<details>

<summary>Screenshots of how to do (click-me !)</summary>

![Add organism column](/metexplore-web/metexplore-training/images/biosourceAddOrganismColumn.png)

*Add the organism column in the BioSource grid*

![Search in the organism field](/metexplore-web/metexplore-training/images/biosourceSearchOrganism.png)

*Selection of a BioSource by searching a specific organism in MetExplore*

</details>

{{< nextButton link="BioSourceSearchComboBox" >}}
