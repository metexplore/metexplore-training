---
tags: ["docs","biosource", "tuto"] 
title: "BioSource search based on grouping"
linkTitle: "BioSource search based on grouping"
weight: 6
---



{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

By default, grouping is done using organisms. You can change the way BioSources are classified in the BioSource panel. 

{{% tutoParagraph %}}

- Change the "Group by" field: you can select one of the grouping options: Organism, Source Database, Database Type, Status or Projects. Use one of them to find the BioSource that we want to explore ([reminder]({{< relref "_index.md#biosourceTuto" >}})).
- Once you have found your BioSource of interest in the list, you need to select it to load all the content of the BioSource and start working on it.
To do so you can:
  - Right click on the line and click on "Select BioSource"
  - Double click on the line

{{% /tutoParagraph %}}

<details>

<summary>Screenshot of how to do (click-me !)</summary>

![BioSource Grouping](/metexplore-web/metexplore-training/images/biosourceGrouping.png)

*MetExplore BioSources grid grouped by organism*

</details>

You can remove the grouping by unchecking the "Group table" check box.

{{< nextButton link="BioSourceSearchField" >}}