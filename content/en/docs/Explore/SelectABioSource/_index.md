---
tags: ["docs","biosource"] 
title: "Select a BioSource"
linkTitle: "Select a BioSource"
weight: 5
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

BioSources are classified using the following fields:

- **Organism:** all BioSources corresponding to the same organism.
- **Source Database:** the database hosting the BioSource (e.g. Biomodels, MicroCyc …)
- **Database Type:** the generic provenance of a BioSource. It can for now be only of three kinds: KEGG, BioCyc or SBML.
- **Status:** kind of visibility of a BioSource. Public means that the BioSource is accessible to anyone using MetExplore. Private means that the BioSource is owned by someone registered in MetExplore. This BioSource can be private but also shared with a selection of collaborators.
- **Projects:** when you are the owner of several BioSources, you can group them in projects (e.g. all your human metabolic networks).

As data from our example dataset have been obtained on human cerebrospinal fluid, we will focus on the human genome-scale metabolic network. There are 13 public metabolic networks for *Homo sapiens* in MetExplore. These biosources are either imported from SBML files provided in an article or issued from different databases: KEGG, BioCyc and specific databases for human models such as VMH ([Virtual Metabolic Human](https://www.vmh.life/)), HMA ([Human Metabolic Atlas](http://www.metabolicatlas.com/)). Note that some of the biosources available for *Homo sapiens* are tissue specific (as specified in "Strain").

<a id="biosourceTuto"></a>

{{% importantParagraph %}}
In this practical, we will use the human metabolic network Recon2.2, which has been published in 2016 by Swainston et al.[^1] and is an update (partly manually curated) of the Recon2 metabolic network published by Thiele et al. in 2013[^2].
{{% /importantParagraph %}}

<br/>

The first thing to do before starting working on a metabolic network is to select the right BioSource. In MetExplore, this task can be achieved in different ways.

- [BioSource search strategy based on grouping]({{< ref BioSourceSearchGrouping >}})
- [BioSource search strategy based on field search]({{< ref BioSourceSearchField >}})
- [BioSource search with the combo box]({{< ref BioSourceSearchComboBox >}})

Once a BioSource is selected, all the panels will be populated with the number of elements displayed.

![Grid headers](/metexplore-web/metexplore-training/images/headerGrids.png)

*Grid headers displaying numbers of metabolic elements after selected a BioSource*

Moreover, the right panel will contain all information related to the BioSource.

![BioSource information](/metexplore-web/metexplore-training/images/bioSourceInfo.png)



[^1]: Swainston N, Smallbone K, Hefzi H, Dobson PD, Brewer J, Hanscho M, et al. Recon 2.2: from reconstruction to model of human metabolism. Metabolomics. 2016;12: 109. doi:10.1007/s11306-016-1051-4

[^2]: Thiele I, Swainston N, Fleming RMT, Hoppe A, Sahoo S, Aurich MK, et al. A community-driven global reconstruction of human metabolism. Nat Biotechnol. 2013/03/05. 2013;31: 419–425.



