---
# categories: ["Examples", "Placeholders"]
tags: ["docs","biosource"] 
title: "What is a BioSource?"
linkTitle: "What is a BioSource?"
weight: 4
---

{{% info %}}

Several genome scale metabolic networks can be available for a given organism. In MetExplore, we use the term "BioSource" to refer to a metabolic network. The aim is to avoid confusion and keep the flexibility of having several networks for several organisms. Hence, a BioSource will have a MetExplore-specific identifier called "Biosource id".
MetExplore provides access to networks imported from public databases ([BioCyc](https://biocyc.org/)[^1], [KEGG](https://www.genome.jp/kegg/)[^2], [BioModels](https://www.ebi.ac.uk/biomodels/)[^3] …) and from SBML files[^4] [^5]. All of these files are present in the "public BioSource" repository. Note that you can also register and upload your own SBML files. You will then be the only one able to access these networks and will have the opportunity to share them with others.

{{% /info %}}

[^1]: Karp PD, Billington R, Caspi R, Fulcher CA, Latendresse M, Kothari A, Keseler IM, Krummenacker M, Midford PE, Ong Q, Ong WK, Paley SM, Subhraveti P. The BioCyc collection of microbial genomes and metabolic pathways. Brief Bioinform. 2019 Jul 19;20(4):1085-1093.

[^2]: Kanehisa M, Furumichi M, Sato Y, Kawashima M, Ishiguro-Watanabe M. KEGG for taxonomy-based analysis of pathways and genomes. Nucleic Acids Res. 2023 Jan 6;51(D1):D587-D592.

[^3]: Rahuman S Malik-Sheriff and others, BioModels—15 years of sharing computational models in life science, Nucleic Acids Research, Volume 48, Issue D1, 08 January 2020, Pages D407–D415

[^4]: Hucka M, Finney A, Sauro HM, Bolouri H, Doyle JC, Kitano H, et al. The systems biology markup language (SBML): a medium for representation and exchange of biochemical network models. Bioinformatics. 2003;19: 524–31.

[^5]: Hucka M, Smith LP. SBML Level 3 package: Groups, Version 1 Release 1. J Integr Bioinform. 2016;13: 290. doi:10.2390/biecoll-jib-2016-290

{{< nextButton link="SelectABioSource" >}}