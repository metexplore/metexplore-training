---
tags: ["docs","grid", "tuto"] 
title: "Find information about metabolic entities"
linkTitle: "Find information about metabolic entities"
weight: 5
---

Information about each network elements (metabolite, reaction, gene …) can be accessed by clicking on the information symbol next to the element name. This will open a popup window, containing information about:

- for reactions: equation, associated genes and associated pathways

{{% tutoParagraph %}}

- How many genes are associated with the enolase reaction?
- Display the GPR (Gene-Protein-Reaction) association scheme of the enolase reaction
- What are the substrates and the products of the enolase reaction?
- How many pathways is enolase involved in?

{{% /tutoParagraph %}}

<details>

<summary>Information about enolase (click me!)</summary>

![Enolase information](/metexplore-web/metexplore-training/images/enolase.png)

</details>

- for metabolites: the identifiers from different databases (if available in the initial SBML)

{{% tutoParagraph %}}

- Display info about cytosolic butyrate (M_but_c)

{{% /tutoParagraph %}}

<details>

<summary>Information about butyrate (click me!)</summary>

![Enolase information](/metexplore-web/metexplore-training/images/butyrate.png)

</details>

{{< nextButton link="FilterGrids" >}}
