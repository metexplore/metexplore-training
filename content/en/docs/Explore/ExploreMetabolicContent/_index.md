---
tags: ["docs","grid", "tuto"] 
title: "Explore the metabolic content of a BioSource"
linkTitle: "Explore the metabolic content of a BioSource"
weight: 6
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

The content of the metabolic network is displayed in grids, under the **Network Data** tab.


{{% info %}}

**What is a Grid?**

The concept of **grid** in MetExplore corresponds to a spreadsheet-like representation of the content of the various sets used to describe a metabolic network. Hence, there will be a grid for cellular compartments (**Compartments**), metabolic pathways (**Pathways**), reactions (**Reactions**), metabolites (**Metabolites**) up to genes (**Genes**).

Grids come with several features (e.g. filters) and are editable if you are working on your own private BioSource.
{{% /info%}}

{{% tutoParagraph %}}


- Open the metabolite grid
- Delete the following columns: 
  - Side compound
  - Compartment
- Add the following columns:
  - chebi
  - inchi
  - pubchem
- Rearrange the columns (drag & drop) to get this order:
  - Identifier
  - Name
  - Formula
  - chebi
  - inchi
  - pubchem
{{% /tutoParagraph %}}

<details>

<summary>Screenshot of how to do (click-me !)</summary>

![Adding and removing columns](/metexplore-web/metexplore-training/images/addRemoveColumns.png)

*Add and remove columns in a grid*

</details>

Data in each column can also be searched for, by using the "Search" field. The data in the table will be filtered depending on the values contained in this column. Note that this search is not propagated to the other grids (unlike with the "filter" option, see next section). 

Note that in the "Metabolites" grid, you have the option to perform a "fuzzy" search on the metabolites names. 
The aim of fuzzy search is to more easily find metabolites. Instead of searching for the exact sub-string entered in the search box, it retrieves metabolites with names that do not exactly contain this sub-string, but a close one. This "fuzzy" search uses the Levenshtein distance, which is calculated as the minimum cost to transform the word M into P by performing the some elementary operations (substitution, insertion, and/or deletion of a character).
To switch from "exact" to "fuzzy" search, click on the magnifying glass next to the search box.

{{% tutoParagraph %}}

- Do a fuzzy search of the term "lactic acid" in the name column
- Do an exact search of the same term
 
{{% /tutoParagraph %}}

<details>

<summary>Screenshot of how to do (click-me !)</summary>

![Metabolite search](/metexplore-web/metexplore-training/images/metaboliteSearch.png)

*Fuzzy search of lactic acid in the metabolite names*

</details>

{{< nextButton link="DisplayInfo" >}}


