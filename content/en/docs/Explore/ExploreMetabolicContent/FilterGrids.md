---
tags: ["docs","grid", "tuto"] 
title: "Filter grids"
linkTitle: "Filter grids"
weight: 7
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

{{% info %}}

### The **filter** concept

All grids in MetExplore (e.g. metabolites, reactions …) are dynamically connected, meaning that interactions on one grid can affect the content of other grids. The central notion for this kind of interaction is the notion of "filter". **A filter on a line or a set of lines in one grid will imply a selection on the other grids**. For instance, by filtering on a Pathway in the pathway grid, one can select all the reactions corresponding to this pathway and see them displayed in the Reactions grid. Similarly, filtering on one reaction in the "Reaction" grid will select the elements corresponding to this reaction in the other grids: substrates and products of the reaction, genes required to encode the enzyme catalyzing the reaction...

Note that filters can be applied to a selection of several elements, i.e., a list of metabolites or reactions for instance.

{{% /info %}}

To **apply a filter**, right click on a line or a selection of lines and select "new filter on selection". The number of elements selected in each grid appears in the tab above the grid.

To **cancel a filter**, erase the content of the "Search" field or right-click on the grid and click on "Delete Filter & Search".

{{% tutoParagraph %}}

- Open the gene grid
- Select the genes HGNC:1047 and HGNC:1048
- Filter on this selection
- Which pathways are these genes involved in?

{{% /tutoParagraph %}}

<details>

<summary>Filtering on HGNC:1047 and HGNC:1048 (click-me !)</summary>

![Filtering genes 1](/metexplore-web/metexplore-training/images/filteringGenes1.png)

![Filtering genes 2](/metexplore-web/metexplore-training/images/filteringGenes2.png)

*MetExplore filtering on genes*

</details>

Once a filter has been applied, it appears **in the "Filters" tab** on the right panel. 

It is possible to **perform several successive filters**. When you perform successive filters without deleting the previous ones, the new filters are applied in addition to the previous ones. For instance, after performing a first filter on genes (See screenshot below), if you filter on one specific pathway, this will select, among the reactions associated with the genes, only the ones that are part of the filtered pathway. All the successive filters performed are tracked in the "Filters" tab. From this tab, you can chose to remove all filters at once or only some specific ones.

![Filter tab](/metexplore-web/metexplore-training/images/filterTab.png)

*MetExplore multiple successive filters (on gene and pathway)*

{{< nextButton link="MappingMetabolomics" >}}
