---
tags: ["docs","visualisation", "metabolomics", "tuto"] 
title: "Visualise mapped data"
linkTitle: "Visualise mapped data"
weight: 6
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/visualise" >}}

### Objectives

In this section, we will show

1. how to create a network which is the union of a few selected metabolic pathways of interest
2. how to visualize it
3. how to highlight mapped metabolites. This requires to first put all the reactions belonging to at least one of the pathways of interest in the cart and then move to the visualizing tool to view reactions and mapped metabolites.

{{% info %}}

#### The "Cart" concept

To interactively create and visualize networks based on selection of reactions (e.g. the union of all reactions belonging to specific pathways or all reactions taking place in the mitochondria), MetExplore use the concept of "Cart", as any online shopping system. The Cart content is displayed in the right panel of MetExplore.

{{% /info %}}

### Create a subnetwork of interest based on pathways

In this first sub-section, we will learn how to select a group of reactions belonging to pathways of interest, and put them in the "cart" for visualization.

Following the example of HE signature, we would ideally like to create a subnetwork including all the pathways that contain at least one of the mapped lipids from the dataset. 
These pathways can be identified in the "Pathways" grid by selecting all the pathways with a "nb of mapped" value higher than 0 and with a significant BH-corrected p-value.

{{% tutoParagraph %}}

From the mapping perfomed in the previous section:

- Select the pathways with significant p-value (hold CTRL or SHIFT for multiple selection) except Transport extracellular
- Right click and filter these pathways ( "filter on selection")
- Bonus: Export the grids in Excel File (Export - Export network to Excel). Note that the Excel file will contain exactly the same data (filtered) as the current MetExplore grids

{{% /tutoParagraph %}}

**All the grids are now filtered to display only the elements belonging to these pathways.**

This can be observed in the numbers displayed at the top of each grid. The first number is the number of elements corresponding to the filter (e.g., 975 reactions are present in the 13 selected pathways). The second number is the total number of the given element in the network (e.g. 7785 reactions).

![Filter by pathways](/metexplore-web/metexplore-training/images/filterByPathways.png)

We will visualize all the 975 reactions belonging to these 13 pathways.

{{% tutoParagraph %}}

- Go to the "reactions" grid
- Right click on the reactions list and select "copy all to cart"

{{% /tutoParagraph %}}

<details>

<summary>How to copy reactions to Cart? (click me!)</summary>

![Copy to carts](/metexplore-web/metexplore-training/images/copyToCart.png)

</details>

Note that you can filter out only some specific reactions by selecting them and choosing "copy selection to cart".

All the 975 reactions belonging to the selected pathways are now displayed in the "Cart", in the right panel.

### Visualise the selected network

The aim of this second sub-section is to display the sub-network formed by all the selected reactions.

#### Global visualisation

**Visualization can be achieved using the "Network viz" panel.**

In MetExplore, visualization of a network or sub-network can be achieved either by importing a json file (e.g., saved network visualization from a previous MetExplore session) or from a list of reactions selected in the biosource grid and put into the cart.

**In this section, we will perform the visualization based on the reactions present in the cart.**

{{% tutoParagraph %}}

Go to the "Network viz" panel and click on the "MetExplore selection" button.

{{% /tutoParagraph %}}

![Visualisation of the sub network](/metexplore-web/metexplore-training/images/viz.png)

*Visualization of the selected metabolic pathways in MetExplore*

{{% tutoParagraph %}}

- Click on the play/pause button to pause or restart the animation.
- Move the drawing or zoom in or out using either the mouse wheel or using the 3 buttons at the top right corner.

{{% /tutoParagraph %}}

**In the visualization, reactions and metabolites are displayed as nodes: reactions with rectangles and metabolites with circles.** The edit mode allows modifying the form, the size and the labels of the nodes. These features will not be developed in this tutorial, but detailed information can be found in the MetExplore Viz documentation. 

#### Duplication of side compounds

You notice that some nodes create hubs in the network, as they are connected to many others nodes. These nodes correspond to metabolites such as H2O, proton …, which are involved in many metabolic reactions. They are called "**side-compounds**". To ease the visualization it is recommended to either remove or duplicate the side compounds. Note that considering that a metabolite is a side compound might sometime depend on the reaction, so that side compounds are not defined and removed *a priori*.

{{% tutoParagraph %}}

Duplicate a side compound directly from the visualization window:

- Select the metabolite you identified as being a side compound (e.g., proton)
- Right click and select "Duplicate nodes as side compounds" → "this node" (you can also remove it)
  
{{% /tutoParagraph %}}

<details>

<summary>How to duplicate side compounds?(click me!)</summary>

![Duplicate side compounds](/metexplore-web/metexplore-training/images/duplicationSideCompounds.png)

*Duplication of side-compounds metabolites in the MetExplore visualization*

</details>

You can reiterate this process for each metabolite that you consider as side compound.

A faster way is to specify metabolites as side compounds in the "metabolites" grid and then duplicate or remove them all at once.

{{% tutoParagraph %}}

In the "metabolites" grid, check the box in the "side compound" column for the metabolites you consider as side compounds.

Some metabolites have been defined as side compounds a priori for this network (ATP, H2O, H ...).

In the "network viz" tab, click on "Drawing" → "Duplicate side compounds"

Play animation to automatically update the layout.

You should get a much less dense network.

{{% /tutoParagraph %}}

<details>

<summary>Define side-compounds from metabolite grid and duplicate them(click me!)</summary>

![Define side compounds](/metexplore-web/metexplore-training/images/defineSideCompounds.png)

*Defining and duplicating side-compounds metabolites from the metabolites grid in MetExplore*


![Network with duplicated side compounds](/metexplore-web/metexplore-training/images/vizWithoutSideCompounds.png)

*MetExplore visualization of selected metabolic pathways with duplicated side-compounds*

</details>

### Highlight mapped data

**The aim of this last sub-section is to display the HE dataset metabolites on the selected and visualized subnetwork.**

The mapping on the dataset metabolites has been done in the previous section and can now directly be accessed in the "Network Viz" tab.

{{% tutoParagraph %}}

- Expand the **"Network Manager"** menu in the left panel 
- Select the "Metabolite" tab within the "Styles" tab
- To change the background color of the mapped metabolites, click on the square in the Mapping column of the "Node background" row:
  - Select your mapping and your condition in the 1st drop-down menu
  - Select the "discrete" to select colors according to the three possible values of the average distance.

{{% /tutoParagraph %}}

Colors will be assigned to nodes according to the numerical values and depending on the data type: 

- for discrete values, a color is assigned to each value;
- for continuous values, a color gradient (by default, from yellow to blue) is created and each node is assigned a color accordingly, using a linear scaling.
- If you do not have condition values in your dataset or to apply the same color to all mapped nodes independently on the values, select "as selection": all the nodes will be highlighted with the same color.

<details>

<summary>
How to highlight mapped data onto the network
</summary>

![Highlight mapped data](/metexplore-web/metexplore-training/images/highlightMappedData.png)

*Highlight mapped metabolites in the visualized sub-network*

Mapped metabolites are now highlighted in the subnetwork (filled circles), with colors corresponding to the values of the chosen condition.

![Highlighted mapped data](/metexplore-web/metexplore-training/images/vizWithMappedMetabolites.png)

*Sub-network with metabolites colored according to the average distance found by the matcher identifier*

</details>

{{% tutoParagraph %}}

- Select Width - Mapping in the Styles panel
- Select the mapping
- Select "Identified in Mapping"
- Enter 50 as value.
- Do the same thing for the Height

{{% /tutoParagraph %}}

<details>

<summary>Change the size of mapped metabolites (click me!)</summary>

![Change size](/metexplore-web/metexplore-training/images/changeSizeMappedNodes.png)

*Change size of mapped data*

</details>

### Extract sub-network

{{% tutoParagraph %}}

Click on "Mining" - "Extract Subnetwork" - "From mapping" and select your mapping, to keep only the union of all lightest paths between each pair of identified metabolites.

{{% /tutoParagraph %}}

{{% warning %}}

Subnetwork extraction is cpu and time consuming! Please be patient.

{{% /warning %}}

<details>

<summary>Extract sub-network (click me!)</summary>

![Change size](/metexplore-web/metexplore-training/images/extractSubNetwork.png)

*Extract sub-network from mapped data*

</details>

### Highlight cellular compartments and cellular pathways

Metabolites (circles in the representation) have different colors. This color correspond to the cellular localization of the metabolite (cytoplasm, mitochondria …). **You can also highlight the compartments by drawing convex hull around them.**

{{% tutoParagraph %}}

- Expand the "Compartments" sub-panel on the left hand side, in the "Network Manager" panel.
- Click on "Highlight compartments".

{{% /tutoParagraph %}}

You can similarly **highlight the metabolic pathways with hulls or different colors on the reaction links.**

{{% tutoParagraph %}}

- Expand the "Pathways" sub-panel on the left hand side, in the "Network Manager" panel.
- Click on "Highlight pathways on links".

{{% /tutoParagraph %}}

It is also possible to display only some pathways by selecting only these pathways in the list of pathways.

![Color compartments and pathways](/metexplore-web/metexplore-training/images/colorCompartmentsAndPathways.png)

*Compartments are surrounded by colored hulls and edges are colored according to the pathways*

If you are completely lost, you can visualise the final network [here](
https://metexplore.toulouse.inrae.fr/userFiles//metExploreViz/index.html?dir=/64df65ab9b7c4db68e5c23a7351557f7/networkSaved_20634084)


{{% info %}}

**Congratulations!**

You've finished this tutorial. MetExplore still offers plenty of other possibilities, especially if you create a MetExplore account:

- Import and Export your own networks
- Edit them with several collaborators
- Save and share your visualisations
- Run simple flux balance analyses
- and lot more to discover in the [MetExplore documentation](https://metexplore.toulouse.inrae.fr/metexplore-doc/).


{{% /info %}}


