---
title: "Datasets"
linkTitle: "Datasets"
weight: 2
description: >
  Datasets used in this tutorial
---

<h2>Dataset for Metabolomics data Mapping</h2>


In this tutorial we will use a cerebrospinal fluid metabolic fingerprint of the hepatic encephalopathy disease described in [Weiss et al., 2016](https://europepmc.org/article/med/27520878). Hepatic encephalopathy (HE) corresponds to the neurological or neuropsychological symptoms of acute or chronic liver failure and/or portosystemic shunt. The spectrum goes from mild neuropsychological symptoms to impaired level of consciousness, often leading to coma. Even if the physiopathology is still largely unrevealed, the major role of hyperammonemia in conjunction of inflammation is [well established](https://europepmc.org/article/med/27520878). As a consequence, glutamine levels increase in the brain. However, the sole abundance of ammonia does not scale with symptoms’ severity and it has been shown that associated inflammation, increased levels of TNF-alpha and IL-6, were much better correlated to symptoms’ severity.
The metabolic fingerprint used in this practical corresponds to the metabolites annotated at level 1 (MSI standard) and with Mann & Withney p-value <0.05 between control patients and patients with HE. The metabolic fingerprint contains 28 metabolites.
Since these data were obtained on human samples, we will work on the [human genome-scale metabolic network Recon2.2](https://link.springer.com/article/10.1007/s11306-016-1051-4). We will exemplify grid usage and exploration of network elements using this network.

{{% tutoParagraph %}}

Click **[here](/metexplore-web/metexplore-training/data/HE-fingerprint.xlsx)** to download the list of the 28 metabolites ("Initial list" tab).

{{% /tutoParagraph %}}

<h2>Dataset for Flux analysis</h2>

For the flux analysis part, we will use a smaller model, easier to apprehend: the [ecoli core model](https://systemsbiology.ucsd.edu/Downloads/E_coli_Core), already used in several [flux analyses tutorials](https://opencobra.github.io/cobratoolbox/stable/tutorials/index.html).

{{< nextButton link="GettingStarted" >}}