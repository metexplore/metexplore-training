---
tags: ["docs", "flux"] 
title: "Flux balance analysis"
linkTitle: "FBA"
weight: 9
description: Perform simple flux balance analysis in MetExplore
---


### Objectives

The aim of this section is to learn how to launch and visualise flux analyses in MetExplore.

{{< nextButton link="ImportModel" >}}