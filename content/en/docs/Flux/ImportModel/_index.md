---
tags: ["docs","flux", "tuto"] 
title: "Import a BioSource in MetExplore"
linkTitle: "Import a BioSource"
weight: 1
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/importandexport" >}}

In this tutorial, we will import the ecoli core model from the BIGG database. Indeed, you can launch flux analyses on public BioSources, but you couldn't change the model, especially lower and upper bounds.

{{% tutoParagraph %}}

- If not done, register in MetExplore and login
- Go to http://bigg.ucsd.edu/models/e_coli_core
- Download the e_coli_core.xml file
- Import it in MetExplore (see doc link above)
- Once the job is finished, click on the Result icon

{{% /tutoParagraph %}}

{{< nextButton link="../BrowseModel" >}}
