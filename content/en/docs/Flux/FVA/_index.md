---
tags: ["docs","flux", "tuto", "fva"] 
title: "Perform a flux variability analysis in MetExplore"
linkTitle: "Flux Variability Analysis"
weight: 4
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/flux" >}}


{{% tutoParagraph %}}

- Launch a FVA with maximizing the biomass reaction
- What are the reactions that have not the same min and max values ?
- Launch a second FVA with minimizing the Flux Sum as second objective. What are the differences ?
- Launch a third FVA 

{{% /tutoParagraph %}}

<details>
<summary>Answers (click me!)</summary>

- Sort the reactions according to their flux values. The reactions R_SUCDI and R_FRD7 can take infinite flux values.
- By minimizing the flux sum, you avoid futile cycles in the FBA and FVA answers

</summary>

</details>

{{< nextButton link="../Kos" >}}
