---
tags: ["docs","flux", "tuto", "fba"] 
title: "Perform a flux balance analysis in MetExplore"
linkTitle: "Flux Balance Analysis"
weight: 3
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/flux" >}}


{{% tutoParagraph %}}

- Launch a FBA with maximizing the biomass reaction
- What is the optimal growth rate?
- Select the reactions that have a non-zero value in the first FBA
- Launch the visualisation of the sub-network composed by these reactions (see (doc)[https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/visualise])
- Remove side compounds such as H+, ATP, ADP, etc... and the biomass reaction to have a clearer representation.
- Relaunch the animation to place optimally the nodes
- Map the value of this first FBA on the links (flux in Network Manager)
<!-- - If you are logged in, export the visualisation to share with your colleagues -->
- Do a second FBA by setting the gene G_b0727 gene as gene ko. What is the new optimal growth rate ? What are the changes in the fluxes ?
- Do a third FBA by removing the gene ko and setting the pyruvate dehydrogenase reaction as reaction ko. What is the new optimal growth rate ? What are the changes in the fluxes ?
- Change the lower and upper bounds of glucose and glutamate so that the model only allows incoming flux of glutamate. Be careful, don't forget to change the reversibility and to save your changes ! What is the optimal growth rate when the carbon source is glutamate ?
- Export the results in Excel file


{{% /tutoParagraph %}}

<details>
<summary>Answers (click me!)</summary>

- The optimal growth rate corresponds to the value of the flux of R_BIOMASS_Ecoli_core_w_GAM computed by FBA : 0.8739 hr-1.
- Sort the reactions by their values computed by FBA and select (use the CTRL and SHIFT keys) the reactions that have non-zero flux values
- Right-click on these reactions and add the selection to the cart
- Into the cart, right-click and select "Create Network in viz from Cart"
- Remove the side compounds the most connected and launch the animation to move the nodes automatically
- In Network Manager, click on the flux tab, select the mapping, select One and fba, check Distribution Graph and Add Values on network.

![alt text](image.png)

- Select genes or reactions in the ko_genes or ko_reactions combobox of the FBA form to perform knock-outs.
- The optimal growth rate with glutamate as carbon source is 0.599 hr-1. You can edit directly the lower, upper bounds, and reversibility status in the grid but you have not to forget the Save button. The changes are saved in the MetExplore database.

</details>

{{< nextButton link="../FVA" >}}


