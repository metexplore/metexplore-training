---
tags: ["docs","flux", "tuto", "kos"] 
title: "Identify key genes and reactions by doing successive knock out"
linkTitle: "Reaction or Gene KOs"
weight: 5
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/flux" >}}

{{% tutoParagraph %}}

- Identify the essential reactions and genes of E.coli by using the Knock out analysis 

{{% /tutoParagraph %}}


<details>
<summary>Answers (click me!)</summary>

- Launch the analysis and browse the results on the reaction or gene grid to identify reactions or genes for which the ko makes the growth rate to be equal to 0.

</summary>

</details>
