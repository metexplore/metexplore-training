---
tags: ["docs", "flux"] 
title: "Browse a model"
linkTitle: "Browse a model"
weight: 2
---

{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/explore" >}}

{{% tutoParagraph %}}

- Open the ecoli core model that you imported in the previous step or the [public MetExplore instance](https://metexplore.toulouse.inrae.fr/metexplore2/?idBioSource=6745)
- Use the MetExplore grids (see doc link above) to answer these questions:
- What is the number of reactions? of metabolites?
- Search for the id of the biomass reaction
- Display only the reactions that involve ATP (use the smart filters of MetExplore). How many are they?
- Remove the filter used before
- Find the reaction associated to the gene b0008. What is its identifier?
- Display the scheme of the Gene-Reaction association of the reaction R_AKGDH. 
- What are the compartments taken into account in the model?
- What are the exchange reactions that do not constrain the incoming flux of the corresponding metabolites ? Note : exchange reactions' identifiers start by R_EX 
- What are the exchange reactions in which the metabolite is prevented from entering the cell?
- What are the only carbon sources allowed to enter into the cell?
- What internal reactions are constrained?


{{% /tutoParagraph %}}


<details>
<summary>Answers (click me!)</summary>

- The number of reactions and other metabolic entities is diplayed in the headers of the grids of the Network Data tab. This model contains 95 reactions and 72 metabolites.
- To find the identifier of the biomass reaction, you can use the search field in the name and identifier headers of the reaction grid.
- To display only the reactions that involve ATP, find ATP in the Metabolites grid, right-clik on the row and select filter on selection. All the grids will be filtered according to this. 13 reactions involve ATP.
- To remove filters, right-click on a grid and do "Delete Filter & Search"
- To find the reactions associated to a gene, go in the gene Grid, find the gene and perform the filter as explained above. The reaction corresponding to b0008 is R_TALA.
- To display the Gene Reaction Scheme, select a reaction, click on the information button and click on GPR (Gene-Protein-Reaction) association viz tab.

![alt text](GPR.png)


- To display only exchange reactions, filter the table with the search field with the prefix R_EX
- The reactions not constrained for the incoming fluxes are those that have an "infinite" (here -1000) lower bound. You can sort the reactions by clicking on the header of the Flux Lower Bound column. NH4, CO2, H+, H20 O2, and Pi incoming fluxes are not constrained. 
- To know which carbon sources are allowed to enter in the cell, look at the exchange reactions involving carbon sources and their flux lower bound. Here, only Glc and CO2 are allowed to enter into the cell.
- To find the internal reactions that are constrained, remove the R_EX filter and sort the reactions according to their lower and upper bounds. By doing this, you will find the R_ATPM (ATP maintenance) reaction is constrained.


</details>


{{< nextButton link="../FBA" >}}



