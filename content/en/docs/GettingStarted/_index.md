---
# categories: ["Examples", "Placeholders"]
tags: ["registration","docs"] 
title: "Getting Started"
linkTitle: "Getting Started"
weight: 3
description: >
  How to start using MetExplore?
---

<!-- {{% pageinfo %}}
This is a placeholder page that shows you how to use this template site.
{{% /pageinfo %}} -->

To start using MetExplore

- Go to the [MetExplore homepage](http://www.metexplore.fr) 

![MetExplore Home Page](/metexplore-web/metexplore-training/images/metexploreHomePage.png)

*MetExplore Home Page*

- From this home page, you can access the MetExplore documentation, the MetExploreViz documentation, the MetExplore webservices and the registration page.
- click on the "START MetExplore" button

{{< nextButton link="Explore" >}}