---
title: "Overview"
linkTitle: "Tutorial"
weight: 20
menu:
  main:
    weight: 20
---

Welcome to the MetExplore tutorial!

The aim of this document is to guide you through the use of MetExplore web server. It will not cover all the functionalities MetExplore can achieve but it will provide you with the basic concepts and functions. The present document and practical focus on metabolomics data mapping and visualization.

Some graphical conventions are used to ease the use of this tutorial.

{{% tutoParagraph %}}

Practical parts and tasks to do are displayed with this style.

{{% /tutoParagraph %}}


The solutions of the exercices are hidden but you can see them by clicking on the section.

Example:

<details>

<summary>Hidden solution (click-me !)</summary>

This is the solution of the exercise.

</details>

{{% info %}}
Definitions of concepts specific to MetExplore software are displayed with this style.
{{% /info %}}

{{% warning %}}
Warning sections are displayed with this style.
{{% /warning %}}

This button, found at the beginning of a section, is a link to the corresponding MetExplore doc: {{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/">}}

{{< nextButton link="Dataset" >}}
