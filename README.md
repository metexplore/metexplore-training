# MetExplore Tutorial

Tutorial website for MetExplore. Build with [Docsy, a Hugo theme module](https://www.docsy.dev/).

## Run in development mode

```bash
hugo serve -D
```

## Update docsy

If you face to docsy issues, you can update it by doing:

```bash
hugo mod get -u github.com/google/docsy
```

## Generation of the gitlab pages

Done automatically by the CI when you push on master.

<https://metexplore.pages.mia.inra.fr/metexplore-training>

No need to change the baseURL variable in config.toml, it's done when launching hugo in the CI.

## How to add new page?

Pages are in the docs directory. Each section corresponds to a sub-directory. The file _index.md in a sub-directory corresponds to the root of the section.

It's important that each page includes such a header:

```markdown
---
tags: ["docs","visualisation", "metabolomics", "tuto"] 
title: "Visualise mapped data"
linkTitle: "Visualise mapped data"
weight: 6
---
```

- The tags facilitate the search in the web site.
- The title will be displayed at the top of the page
- The linkTitle will be displayed in the menu and internal links
- The weight will influence the plage of the page in the menu. The higher this value, the further down the list the page will appear.

See [the first page of the Tutorial part](https://metexplore.pages.mia.inra.fr/metexplore-web/metexplore-training/docs/) to see all the examples.

## Formatting

To format text and pictures, you can use either markdown or html format.

You can add or modify some styles in the css file "assets/scss/_styles_project.scss".

Specific [short codes](https://gohugo.io/content-management/shortcodes/) have been created to format some text sections:

- important paragraphs
- information paragraphs
- practice paragraphs
- warning paragraphs
- doc button
- next button

The short codes are in the layout/shortcodes directory.

## Include hidden results

It's possible to hide some results so that the trainees have to search before watching the solution.

```html

<details>

<summary>a short title on which the trainee can click to see the result</summary>

A paragraph result or an image, whatever, that will be hidden until the trainee click on the summary text.

</details>

```

This will display:
<details>

<summary>a short title on which the trainee can click to see the result</summary>

A paragraph result or an image, whatever, that will be hidden until the trainee click on the summary text.

</details>

## Include doc button

If a corresponding MetExplore doc page exists, you can add a link to the page.

```markdown
{{< docButton link="https://metexplore.toulouse.inrae.fr/metexplore-doc/documentation/visualise" >}}
```

## Include practical paragraph


```markdown
{{% tutoParagraph %}}

Explain what the trainee has to do. Be careful, you have to add one empty line before and after the paragraph (especially if there are some references inside the paragraph).

{{% /tutoParagraph %}}
```

## Include information paragraph

```markdown
{{% info %}}

Add definitions of concepts specific to MetExplore software. Be careful, you have to add one empty line before and after the paragraph (especially if there are some references inside the paragraph).

{{% /info %}}
```

## Include warning section

```markdown
{{% warning %}}

Add warning alert. Be careful, you have to add one empty line before and after the paragraph (especially if there are some references inside the paragraph).

{{% /warning %}}
```





